var search = require('./controller/search')


module.exports = function (app) {


    app.get('/getLocations', search.getLocations);

    app.get('/getHotels', search.getHotels);
    // application -------------------------------------------------------------

    app.get('*', function (req, res) {

        res.sendFile(__dirname + '/public/index.html'); // load the single view file (angular will handle the page changes on the front-end)
    });

};