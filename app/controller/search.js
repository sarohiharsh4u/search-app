/**
 * Created by Harsh on 28/01/17.
 */

var request=require('request');

var _ = require('underscore-node');

var getParam = function (req, paramName) {
    var param = (req.body && req.body[paramName]) || req.query[paramName];
    return param;
};

exports.getLocations=function (req, res) {


    var keywords=getParam(req,'keywords')

    var returnObj = {
        status: 'success',
        message: '',
        searchResults: null
    };

    if (!keywords) {
        returnObj.status = 'fail';
        returnObj.message = 'Mandatory, search keywords not provided';
        res.status(200).send(returnObj)
        return;
    }

    // use google places api web service

    var url='https://maps.googleapis.com/maps/api/place/autocomplete/json?input='+keywords+'&components=country:in&types=geocode&language=en&key=AIzaSyD5O4wFEFeL7eDLjOM3y1zfjq7yBeq4us4'

    request(url, function (error, response, body) {
        if(error){
            returnObj.status = 'fail';
            returnObj.message = error;
            returnObj.searchResults=null;
        }
        else{
            returnObj.status = 'success';
            returnObj.message = '';
            returnObj.searchResults=JSON.parse(body).predictions;
        }
        res.status(200).send(returnObj)
    })

}


exports.getHotels=function(req,res){

    var keywords=getParam(req,'keywords')

    var returnObj = {
        status: 'success',
        message: '',
        searchResults: null
    };

    if (!keywords) {
        returnObj.status = 'fail';
        returnObj.message = 'Mandatory, search keywords not provided';
        res.status(200).send(returnObj)
        return;
    }

    var hotelData=[
        {
            name: 'Hotel Mohan International, Pahar Ganj, 2, Arakashan Road, Arya Nagar, New Delhi, Delhi, India',
            price: '1965',
            rating:'3.8'
        },
        {
            name: 'FabHotel Star Delhi Airport ,Mahipalpur, Near IGI Airport, New Delhi, Delhi, India ',
            price: '1549',
            rating:'4.1'
        },
        {
            name: ' FabHotel Casa Friends Colony, Friends Colony East, Near Ashram Chowk, New Friends Colony, New Delhi, Delhi, India',
            price: '2349',
            rating:'4.4'
        },
        {
            name: ' FabHotel Anutham Nehru Place,Hotel Anutham, 10/5, Nehru Enclave, New Delhi, Delhi, India',
            price: '2109',
            rating:'4.0'
        },
        {
            name: ' FabHotel Anutham Saket,E-147, Saket Rd, Block E, Saket, New Delhi, Delhi, India ',
            price: '2899',
            rating:'2.7'
        },
        {
            name: ' FabHotel Oakwoods Serai,1st Cross, New Thippasandra, Geetanjali Layout, Opp. BEML Main Gate, Bengaluru, Karnataka, India',
            price: '2499',
            rating:'4.3'
        },

        {
            name: 'Hotel Monarch International, Near BMTC Bus Stand, 28th C Cross Road, 4th Block, Jayanagara Jaya Nagar, Bengaluru, Karnataka, India',
            price: '2327',
            rating:'3.6'
        },
        {
            name: 'Laika Boutique Stay 51/24 Rathna Avenue Richmond Road | Near Trinity Circle, Bengaluru, Karnataka, India',
            price: '2249',
            rating:'3.7'
        },
        {
            name: 'Hotel Bangalore Gate 9 & 12, Gajanana Towers , Kempe Gowda Road, Opposite Maneka Theatre, Near Mysore Bank Circle, Bengaluru, Karnataka, India',
            price: '2301',
            rating:'4.6'
        }
    ]

    // make case insensitive

    var searchStr=keywords.toLowerCase();




    var matchedObjects = _.filter(hotelData, function (obj) {
        return _.values(obj).some(function (el) {

            var lowerCase=el.toLowerCase()
            return lowerCase.indexOf(searchStr) > -1;
        });
    });

        returnObj.searchResults=matchedObjects



    res.status(200).send(returnObj)

}

// 4567

// 1-2 order

//