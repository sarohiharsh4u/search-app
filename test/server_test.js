/**
 * Created by Harsh on 30/01/17.
 */

var should=require('chai').should();
var expect=require('chai').expect;
var supertest=require('supertest');
var  api= supertest('http://localhost:8080');

describe("search-app",function(){

    describe("Google Place API is called",function(){
        it('should return a 200 response',function(done){
            api.get('/getHotels')
                .send({
                    keywords:'new'
                })
                .set('Accept','application/json')
                .expect(200)
                .end(function(err,res){
                    if(err) return done(err)
                    expect(res.body.status).to.equal('success')
                    done()
                })
        });
    });

    describe("Hotel Name with details API is called ",function(){

        it('should return a 200 response',function(done){
            api.get('/getLocations')
                .send({
                    keywords:'new'
                })
                .set('Accept','application/json')
                .expect(200)
                .end(function(err,res){
                    expect(res.body.status).to.equal('success')
                    done()
                })
        });
    })
})


