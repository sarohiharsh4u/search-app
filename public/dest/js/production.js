var app = angular.module('searchApp', []);

app.factory('searchFactory', ['$http','$q',function($http,$q) {

    return{
        getLocations:function(keywords){

            var request = $http({
                method: "get",
                url: "/getLocations?keywords="+keywords,
                contentType: 'application/json'

            })
            return request
        },

        getHotels:function(keywords,$scope){

            if ($scope.resolved) {
                $scope.cancel();
            }

            $scope.canceler = $q.defer();

            $scope.resolved = true;
            var request = $http({
                method: "get",
                url:"/getHotels?keywords="+keywords,
                contentType: 'application/json',
                timeout: $scope.canceler.promise
            })
            return request
        }
    }
}]);
app.controller('SearchController', ['$scope','$http','searchFactory','$q', function($scope, $http, searchFactory,$q) {



    var getLocations=function(keywords){
        searchFactory.getLocations(keywords).success(function(res){

            if(res.status=='success'){
                $scope.googlePlaces = res.searchResults
            }
            else{
                $scope.googlePlaces = []
                console.log(res.message)
            }
        })
    }

    var getHotels=function(keywords){
        searchFactory.getHotels(keywords,$scope).then(function(res){
            $scope.resolved = false;

            if(res.data.status=='success'){
                $scope.hotels = res.data.searchResults
            }
            else{
                $scope.hotels = []
                console.log(res.data.message)
            }
        })
    }

    $scope.canceler = $q.defer();

    $scope.resolved = false;

    $scope.cancel = function() {

        // cancel ongoing request, if response doesn't come from this request and next request is made

        $scope.canceler.resolve("http call aborted");
    };

    $scope.populateView=function(keywords){

        getLocations(keywords)
        getHotels(keywords)

    }



    $scope.setValue=function(value){

        $scope.search=value
        getLocations($scope.search)
        getHotels($scope.search)
    }


}]);