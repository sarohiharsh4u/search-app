app.factory('searchFactory', ['$http','$q',function($http,$q) {

    return{
        getLocations:function(keywords){

            var request = $http({
                method: "get",
                url: "/getLocations?keywords="+keywords,
                contentType: 'application/json'

            })
            return request
        },

        getHotels:function(keywords,$scope){

            if ($scope.resolved) {
                $scope.cancel();
            }

            $scope.canceler = $q.defer();

            $scope.resolved = true;
            var request = $http({
                method: "get",
                url:"/getHotels?keywords="+keywords,
                contentType: 'application/json',
                timeout: $scope.canceler.promise
            })
            return request
        }
    }
}]);