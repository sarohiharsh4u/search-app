module.exports = function(grunt) {

    // 1. All configuration goes here
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        concat: {
            dist: {
                src: [

                    'public/src/js/app.js',
                    'public/src/js/services/searchFactory.js',
                    'public/src/js/controllers/SearchController.js',

                ],
                dest: 'public/dest/js/production.js'
            },
            css: {
                src: 'public/src/css/*.css',
                dest: 'public/dest/css/style.css'
            }
        },
        uglify: {
            foo: {
                files: [
                    {src: ['public/dest/js/production.js'], dest: 'public/dest/js/production.min.js'}

                ]
            }

        },
        cssmin: {
            css:{
                src: 'public/dest/css/style.css',
                dest: 'public/dest/css/style.min.css'
            }
        }

    });

    // 3. Where we tell Grunt we plan to use this plug-in.
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-css');

    // 4. Where we tell Grunt what to do when we type "grunt" into the terminal.
    grunt.registerTask('default', ['concat','uglify','cssmin']);

};